<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix'=>'home','middleware'=>['auth']],function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('/prescription','ResourceControllers\PrescriptionController');
    Route::resource('/ingridient','ResourceControllers\IngridientController');
    Route::post('/prescription/change',array('as' => 'prescription.change', 'uses' => 'ResourceControllers\PrescriptionController@change'));
    Route::get('prescription/{site}/delete', ['as' => 'prescription.delete', 'uses' => 'ResourceControllers\PrescriptionController@destroy']);
    Route::get('ingridient/{site}/delete', ['as' => 'ingridient.delete', 'uses' => 'ResourceControllers\IngridientController@destroy']);
    Route::get('/parse',array('as' => 'parse', 'uses' => 'HomeController@parse'));
});
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


