@extends('layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 92%;">
        <section class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                            <?php
                            $file = file_get_contents("https://telemart.ua/");
                            echo "<div class='panel-heading'>Категории</div>";
                            for ($i = 0;$i < 35;$i++) {
                                echo(stristr(substr($categories[0][$i],20),'</div>',true).", ");
                            }
                            echo "<div class='panel-heading'>Подкатегории</div>";
                            for ($i = 0;$i < 113;$i++) {
                                echo(substr($subcategories[0][$i],4).", ");
                            }
                            echo "<div class='panel-heading'>Товары</div>";
                            for ($i = 7;$i < 22;$i++) {
                                echo($products[0][$i]).", ";
                            }
                            ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


