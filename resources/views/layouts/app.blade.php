<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminlte.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/font-awesome/css/font-awesome.css') }}" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet">

</head>
                    @guest
                        <nav class="navbar navbar-default navbar-static-top">
                        <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                        </ul>
                        </div>
                        </nav>
                    @else
                        <body class="hold-transition sidebar-mini">
                        <div class="wrapper">
                            <!-- Navbar -->
                            <header class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
                                <!-- Left navbar links -->
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
                                    </li>
                                    <li class="nav-item d-none d-sm-inline-block">
                                        <a href="index3.html" class="nav-link">Home</a>
                                    </li>
                                    <li class="nav-item d-none d-sm-inline-block">
                                        <a href="#" class="nav-link">Contact</a>
                                    </li>
                                </ul>

                                <!-- SEARCH FORM -->
                                <form class="form-inline ml-3" style="    margin-bottom: 0em;">
                                    <div class="input-group input-group-sm">
                                        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                                        <div class="input-group-append">
                                            <button class="btn btn-navbar" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>

                                <!-- Right navbar links -->
                                <ul class="navbar-nav ml-auto">
                                    <!-- Notifications Dropdown Menu -->
                                    <li class="dropdown user user-menu mr-5">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre style="color:black;">
                                            <span class="hidden-xs" style="font-size: 14px;">{{ Auth::user()->name }}</span> <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu mt-3" style="min-width:18px;">
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();" >
                                                    Logout
                                                </a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </header>
                            <!-- /.navbar -->

                            <!-- Main Sidebar Container -->
                            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                                <!-- Brand Logo -->
                                <a  href="{{ url('/home') }}" class="brand-link  heading  skin-blue" style="padding: 0.5rem 1rem;">
                                    <i class=" fa fa-spoon fa-2x ml-2"></i>
                                    <span class="brand-text font-weight-light heading">Книга рецептов</span>
                                </a>

                                <!-- Sidebar -->
                                <div class="sidebar">
                                    <!-- Sidebar user panel (optional) -->
                                    <div class="user-panel mt-3 pb-3   d-flex">
                                        <div class="info ml-2">
                                            <a href="#" class="d-block"><i class="fa fa-user fa-lg" ></i>
                                                 {{ Auth::user()->name }}</a>
                                        </div>
                                    </div>

                                    <!-- Sidebar Menu -->
                                    <nav class="mt-2">
                                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                                            <!-- Add icons to the links using the .nav-icon class
                                                 with font-awesome or any other icon font library -->
                                            <li class="nav-item">
                                                <a href="{{route('prescription.index')}}" class="nav-link  heading ">
                                                    <i class="fa fa-book fa-lg"></i>
                                                    <p>
                                                        Мои рецепты
                                                    </p>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{route('ingridient.index')}}" class="nav-link  heading">
                                                    <i class=" fa fa-puzzle-piece fa-lg"></i>
                                                    <p>
                                                        Мои ингридиенты
                                                    </p>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{route('parse')}}" class="nav-link  heading">
                                                    <i class=" fa fa-puzzle-piece fa-lg"></i>
                                                    <p>
                                                        Парсинг
                                                    </p>
                                                </a>
                                            </li>

                                        </ul>
                                    </nav>
                                    <!-- /.sidebar-menu -->
                                </div>
                                <!-- /.sidebar -->
                            </aside>

                    @endguest
    @yield('content')

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

</html>
