
    <link rel="stylesheet" type="text/css" href="{{ asset('css/adminlte.css') }}" />
        <link rel="stylesheet" href="https://cdnjs.cloudfare.com/ajax/libs/fonr-awesome/4.7.0/css/font-awesome.min.css"/>


</head>
<body>
<div class="wrapper  h-100">
<aside class="main-sidebar sidebar-dark-primary elevation-4" style="z-index: 0;">

    <a style="padding: 1.73rem 1rem;background-color: #367fa9;" href="{{ url('/home') }}" class="brand-link  heading  skin-blue">
        <i class=" fa fa-spoon fa-2x ml-4"></i>
        <span class="brand-text font-weight-light h2 heading">Книга рецептов</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column menu" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item">
                    <a href="{{route('prescription.index')}}" class="nav-link h2 heading ">
                        <i class="fa fa-book fa-lg"></i>
                        <p>
                            Мои рецепты
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('ingridient.index')}}" class="nav-link h2 heading">
                        <i class=" fa fa-puzzle-piece fa-lg"></i>
                        <p>
                            Мои ингридиенты
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('parse')}}" class="nav-link h2 heading">
                        <i class=" fa fa-puzzle-piece fa-lg"></i>
                        <p>
                            Парсинг
                        </p>
                    </a>
                </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
</body>
    <style type="text/css">
        ul.submenu {
            margin-left: 50%;
            margin-top: 10%;
            display: none;
            position: absolute;
            width: 150px;
            top: 37px;
            left: 0;
            background-color: white;
            border: 1px solid #367fa9;
        }
        ul.submenu > li {
            display: block;
        }
        ul.submenu > li > a {
            display: block;
            padding: 10px;
            color: white;
            background-color: #367fa9;
            text-decoration: none;
        }
        ul.submenu > li > a:hover {
            text-decoration: underline;
            background-color: black;
        }
        ul.menu > li:hover > ul.submenu {
            display: block;
        }
    </style>
    <!-- Scripts --

