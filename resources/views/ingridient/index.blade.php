@extends('layouts.app')

@section('content')
    <div class="content-wrapper" style="min-height: 92%;">
        <section class="container">
            @if (session()->has('success'))
                <div><h1 class="mt-2 text-success text-center">{{ session('success') }}</h1></div>
            @endif
            <h1 class="pull-left mt-3" >Ингредиенты</h1>
            <a style="-webkit-margin-before: 2em;-webkit-margin-after: 0.67em;" href="{{route('ingridient.create')}}" class="btn btn-primary pull-right "> Добавить ингридиент</a>
            <table class="table table-striped">
                <thead>
                    <th>Меню</th>
                    <th class="text-right">Действия</th>
                </thead>
                <tbody>
                    @forelse($ingridients as $ingridient)
                        <tr>
                            <td class="align-middle">{{$ingridient->name}}</td>
                            <td class="align-middle">
                                <a href="{{route('ingridient.delete',$ingridient->id)}}" class="btn btn-default pull-right"> <i class="nav-icon fa fa-trash"></i></a>
                                <a href="{{route('ingridient.edit',$ingridient->id)}}" class="btn btn-default pull-right mr-2"> <i class="nav-icon fa fa-edit"></i></a>
                            </td>
                        </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
        </section>
    </div>
@endsection