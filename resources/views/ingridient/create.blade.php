
@extends('layouts.app')
@section('content')
    <div class="content-wrapper h-100 mt-2">
        @extends('layouts.sidebar')
        <div class="container">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-horizontal"  action="{{route($route,$ingridients)}}" method="POST">
                {{method_field($method)}}
                {{ csrf_field() }}
                @include('ingridient.partials.form')
            </form>
        </div>
    </div>
@endsection

