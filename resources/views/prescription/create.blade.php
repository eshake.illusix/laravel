@extends('layouts.app')

@section('content')

    <div class="content-wrapper" style="min-height: 92%;">

        <section class="container"
            <!-- Breadcrumb -->
        <h1 class="mt-2">{{$h1}}</h1>
            <!-- /Breadcrumb -->
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <form class="form-horizontal"  action="{{route($route,$prescriptions)}}" method="POST">
            {{method_field($method)}}
            {{ csrf_field() }}
            @include('prescription.partials.form')
        </form>
            <div id="myModal1" class="modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <span class="close"></span>
                        <div class="order_call_header">
                            <h1 id="popup_text" class="ml-3">Добавление ингредиента</h1>
                        </div>
                        <div class="modal-body">
                            <form  method="POST"  class="consultation" id="contactform">
                                <label for="fio" class="center-block">Название</label>
                                <input type="text" class="form-check mb-3" id="name_ing"><hr>
                                <div class="send col-xs-4 btn btn-secondary pull-right btn-lg" id="new_ing_create"  type="submit">Сохранить</div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </div>
@endsection
