@extends('layouts.app')

@section('content')
    <div class="content-wrapper" style="min-height: 92%;">
        <section class="container">
            @if (session()->has('success')) <div><h1 class="mt-2 text-success text-center">{{ session('success') }}</h1></div> @endif
            <h1 class="pull-left mt-3" >Мои рецепты</h1>
            <a style="-webkit-margin-before: 2em;-webkit-margin-after: 0.67em;" href="{{route('prescription.create')}}" class="btn btn-primary pull-right ">Добавить рецепт</a>
            <table class="table table-striped">
                <thead>
                    <th>Рецепт</th>
                    <th>Описание</th>
                    <th class="text-right">Действие</th>
                </thead>
                <tbody>
                    @forelse($ingridients as $prescription)
                        <tr>
                            <td class="align-middle">{{$prescription->name}}</td>
                            <td class="align-middle">{{$prescription->description}}</td>
                            <td >
                                <a href="{{route('prescription.delete',$prescription->id)}}" class="btn btn-default pull-right"> <i class="nav-icon fa fa-trash"></i></a>
                                <a href="{{route('prescription.edit',$prescription->id)}}" class="btn btn-default pull-right mr-2"> <i class="nav-icon fa fa-edit"></i></a>
                                <a href="{{route('prescription.show',$prescription->id)}}" class="btn btn-default pull-right mr-2"> <i class="nav-icon fa fa-eye"></i></a>
                            </td>
                        </tr>
                    @empty
                    @endforelse
                </tbody>
            </table>
        </section>
    </div>
@endsection
