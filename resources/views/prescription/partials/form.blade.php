
<label class="">Название</label>
<input type="text" class="form-control" name="name" value="{{$prescriptions->name or ""}}">
<label>Описание</label>
<input type="text" class="form-control" name="description" value="{{$prescriptions->description or ""}}">
<hr>

<div class="w-100 p-3 d-block float-right" name="div_ingridients">

    <div id='form_left' class='col-xs-6' >
    <label class="mr-5">Ингридиент</label> <label class='ml-5'>Количество</label><br>
    <?php
        if(!$create) {
            foreach ($ingridients as $key=>$ingridient) {
                echo "<div id='div-".$ingridient->id."'><select class='mb-2 mr-2 sBig'  name='ingridients[]'>";
                foreach ($ingridients_all as $ing) {
                    echo " <option ";
                    if ($ingridient->name==$ing->name) {
                        echo "selected " ;
                    } else {
                        foreach ($ingridients as $ing_dis) {
                            if ($ing_dis->name==$ing->name) {
                                echo " disabled ";
                              }
                        }
                    }
                    echo "value='".$ing->id ."'>".$ing->name."</option>";
                }
                echo "</select>";
                echo "<input class='mt-1 ml-2' style='height:28px;' name='count[]' value='".$counts[$key]->count."'>
               <select class='mb-2 sSmall'  name='dimension[]'>";
                foreach ($dimensions_all as $dim_all) {
                    echo "<option value='".$dim_all->id."'";
                    if ($dimensions[$key]->name==$dim_all->name) {
                        echo"selected";
                    }
                    echo ">".$dim_all->name."</option>";
                }
                echo "</select>
                    <div class='btn btn-default mb-1' id='button-".$ingridient->id."' onclick='DeleteDynamicExtraField(this)'><i class='nav-icon fa fa-times'></i></div></br></div>";
            }
        } else {
            echo "<div> <select class='mr-1 mb-1 mt-1 sBig' name='ingridients[]'>";
                   foreach ($ingridients_all as $ing) {
                       echo " <option ";echo "value='".$ing->id ."'>".$ing->name."</option>";
                   }
                   echo "</select>
                        <input class='mt-1 ml-2 mr-1' style='height:28px;' name='count[]' value=''>
                        <select class='mb-2 mr-1 sSmall' name='dimension[]'>";
                        foreach ($dimensions_all as$dim_all) {
                            echo "<option value='".$dim_all->id."'>".$dim_all->name."</option>";
                        }
                        echo "</select>
                       <div class='btn btn-default mb-1 mr-2 ml-1' onclick='DeleteDynamicExtraField(this)'><i class='nav-icon fa fa-times'></i></div> </br>
                       </div></div>";
        }
        echo "</div>";
    ?>
<div class="w-100 p-3 d-block float-left d">
    <div class="col-xs-3 ">
         <div  class="btn btn-secondary  send" id="end_div" >Добавить</div>
    </div>
    <div class="col-xs-2 mt-2" style="width: 13%;">
        <p>Нет в списке?</p>
    </div>
    <div class="col-xs-2 " style="display: inline-block">
        <a href="#myModal1" class="btn btn-secondary " data-toggle="modal">Создать новый ингредиент</a>
    </div>
</div>
    <div class="w-100 p-3 d-block float-left">
        <div class="col-xs-4 d-inline-block">
            <input class="btn btn-primary btn-lg height-control" type="submit" value="Сохранить рецепт">
        </div>
    </div>
</div >
<script>

    function DeleteDynamicExtraField(element){
        $(element).parent().remove();
    }

    window.onload = function() {
        $('.sBig').select2({width:'125px'});
        $('.sSmall').select2({width:'55px'});
        var button = document.getElementById("end_div");
        var new_ing_create = document.getElementById("new_ing_create");
        var elem_left = document.getElementById("form_left");
        var modal = document.getElementById('myModal1');
        var button_close = document.getElementById("btn_close");
        button.addEventListener("mousedown", function (event) {
            var div = $('<div/>', {
                'class': 'DynamicExtraField',
                'id': "DynamicField",
            }).appendTo(elem_left);
            var select_ing = $('<select/>', {
                'class': 'mr-1 mb-1 mt-1',
                'name' : 'ingridients[]'
            }).html('@foreach($ingridients_all as $ingridient)\n' +
                '<option value="{{$ingridient->id}}">{{$ingridient->name}}</option>\n' +
                '@endforeach') .appendTo(div).prop('required', true);
            $(select_ing).select2({ width: '125px' });
            var select_dim = $('<input/>', {
                'class': 'mt-1 ml-2 mr-2',
                'style':'height:28px;',
                'name' : 'count[]'
            }).appendTo(div).prop('required', true);
            var select_dim = $('<select/>', {
                'class': 'mb-2 mr-1',
                'name' : 'dimension[]'
            }).html('@foreach($dimensions_all as $dimension)\n' +
                '<option value="{{$dimension->id}}">{{$dimension->name}}</option>\n' +
                '@endforeach') .appendTo(div).prop('required', true);
            $(select_dim).select2({ width: '55px' });
            var select_dim_but = $('<div/>', {
                'class': 'btn btn-default  mb-1 mr-2 ml-2',
                'name' : 'delete[]',
                'onclick':'ingredient_new_detach()'
            }).html('<i class="nav-icon fa fa-times"></i>\n').appendTo(div).prop('required', true);
            select_dim_but.click(function() {
                $(this).parent().remove();
            });
        });
        new_ing_create.addEventListener("mousedown", function (event) {
            var ingredient = $('#name_ing').val();
            $.ajaxSetup({
                beforeSend: function(xhr, type) {
                    if (!type.crossDomain) {
                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                    }
                },
            });
            $.ajax({
                type: 'POST',
                url: '{{route('ingridient.store')}}',
                data: {name_ingredient:ingredient},
                success:function(result){
                    jQuery.noConflict()
                    $('#myModal1').modal('hide');
                    modal.style.display = "none";
                }
            });
        });
        window.onclick = function(event) {
            if ((event.target == modal)||(event.target ==button_close)) {
                modal.style.display = "none";
                jQuery.noConflict()
                $('#myModal1').modal('hide');
            }
        }
    }
</script>