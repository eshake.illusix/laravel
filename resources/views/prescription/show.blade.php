
@extends('layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 92%;">
        @extends('layouts.sidebar')
        <div class="container">
            <form>
                <h1 class="mt-1">{{$prescriptions->name}}</h1>
                <p>{{$prescriptions->description}}</p>
                <input   id="id_prescription" style="display:none; "value="{{$prescriptions->id}}">
                <div class="col-md-3"><label>Ингридиент </label></div><label> Количество</label><hr>
                @forelse ($ingridients as $key =>$ingridient)
                    <div id='form_add_left' style='width: 25%; float: left; display:block; margin-bottom: 2%;'>
                        <p>{{$ingridient->name}}<p>  <hr>
                    </div>
                    <div id='form_add_right' style='width: 20%; float: left;display: inline-block;margin-right: 40%;margin-bottom: 2%;'>
                        <div class=""> <input value="{{$ingridient_prescription_count[$key]->count}}"  onchange="edit_save(this,{{$ingridient->id}})" type="text" class="col-xs-3 border-0"> </div>
                        <div class=""> <p > {{$dimensions[$key]->name}}</p><hr> </div>
                    </div>
                @empty
                @endforelse
            </form>
        </div>
    </div>
@endsection
<script>
    function edit_save(element,id_ingredient){
        var id_prescription="<?php echo $prescriptions->id?>";
            $.ajaxSetup({
                beforeSend: function(xhr, type) {
                    if (!type.crossDomain) {
                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                    }
                },
            });
            $.ajax({
                type:'post',
                url:'{{route('prescription.change')}}',
                data:{count_data:$(element).val(),ingredient_id:id_ingredient,prescription_id:"<?php echo $prescriptions->id?>"},
                success:function(result){
                },
                error: function(result){
                    console.log(result);
                }
            });
        }
</script>
