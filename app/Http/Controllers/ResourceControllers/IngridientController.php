<?php

namespace App\Http\Controllers\ResourceControllers;

use App\Ingridient;
use App\Prescription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IngridientController extends Controller
{
    public function index()
    {
        return view('ingridient.index', [
            'ingridients' => Ingridient::all()
        ]);
    }

    public function create()
    {
        return view('ingridient.create', [
            'ingridients' => [],
            'create' => true,
            'route' => 'ingridient.store',
            'method' => 'post'
        ]);
    }

    public function store(Request $request)
    {
        $ingridient = new Ingridient();
        if ($request->name_ingredient == NULL) {
            $this->validate($request, [
                'name' => 'required',
            ]);
            $ingridient->name = $request->name;
            $ingridient->save();
            return redirect('home/ingridient/')
                ->with('success', 'Ингредиент успешно добавлен!');
        } else {
            $ingridient->name = $request->name_ingredient;
            $ingridient->save();
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        return view('ingridient.create', [
            'ingridients' => Ingridient::find($id),
            'create' => false,
            'route' => 'ingridient.update',
            'method' => 'PUT'
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $ingridient = Ingridient::find($id);
        $ingridient->name = $request->input('name');
        $ingridient->save();
        return redirect('home/ingridient')
            ->with('success', 'Ингредиент успешно изменен!');
    }

    public function destroy($id)
    {
        $ingridient = Ingridient::find($id);
        $ingridient->delete();
        return redirect('home/ingridient')
            ->with('success', 'Рецепт успешно удален!');
    }
}
