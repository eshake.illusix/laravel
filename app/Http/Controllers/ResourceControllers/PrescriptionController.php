<?php

namespace App\Http\Controllers\ResourceControllers;

use App\Dimension;
use App\Ingridient_Prescription;
use App\Prescription;
use App\Ingridient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrescriptionController extends Controller
{
    public function index()
    {
       return view('prescription.index', [
            'ingridients' => Prescription::all()
       ]);
    }

    public function create()
    {
        return view('prescription.create', [
            'prescriptions' => [],
            'ingridients_all' => Ingridient::all(),
            'dimensions_all' => Dimension::all(),
            'create' => true,
            'route' => 'prescription.store',
            'method' => 'post',
            'h1' => 'Добавление рецепта'
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);
        $prescription = new Prescription;
        $prescription->name = $request->name;
        $prescription->description = $request->description;;
        $prescription->save();
        foreach ($request->dimension as $key => $val ) {
            $ingridient = Ingridient::find($request->ingridients[$key]);
            $array = [
                'dimension_id' => $val,
                'count' => $request->count[$key]
            ];
            $ingridient->prescriptions()->attach($prescription,$array);
        }
        return redirect('home/prescription/')->with('success', 'Рецепт успешно добавлен!');
    }

    public function show($id)
    {
      return view('prescription.show', [
          'prescriptions' => Prescription::find($id),
          'ingridients' => Prescription::find($id)->ingredients()->orderBy('ingridient_prescriptions.ingridient_id')->get(),
          'dimensions' => Prescription::find($id)->dimensions()->orderBy('ingridient_prescriptions.ingridient_id')->get(),
          'ingridient_prescription_count' => Ingridient_Prescription::prescription_Id($id)->orderBy('ingridient_prescriptions.ingridient_id')->get(),
      ]);
    }

    public function edit($id)
    {
        return view('prescription.create', [
            'prescriptions' => Prescription::find($id),
            'ingridients' => Prescription::find($id)->ingredients()->orderBy('ingridient_prescriptions.ingridient_id')->get(),
            'ingridients_all' => Ingridient::all(),
            'dimensions' => Prescription::find($id)->dimensions()->orderBy('ingridient_prescriptions.ingridient_id')->get(),
            'dimensions_all' => Dimension::all(),
            'counts' => Prescription::find($id)->ingridient_prescriptions()->orderBy('ingridient_prescriptions.ingridient_id')->get(),
            'create' => false,
            'route' => 'prescription.update',
            'method' => 'PUT',
            'h1' => 'Редактирование рецепта'
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'ingridients' => 'required'
        ]);
        $prescription = Prescription::find($id);
        $prescription->name = $request->input('name');
        $prescription->description = $request->input('description');
        $prescription->save();
        foreach ($request->ingridients as $key => $val) {
            $array[$val] = [
                'dimension_id' => $request->dimension[$key],
                'count' => $request->count[$key]
            ];
        }
        $prescription->ingredients()->sync($array);
        return redirect('home/prescription')
            ->with('success', 'Рецепт успешно изменен!');
    }

    public function destroy($id)
    {
        $precription = Prescription::find($id);
        $precription->delete();
        return redirect('home/prescription')
            ->with('success', 'Рецепт успешно удален!');
    }

    public function change(Request $request)
    {
        Ingridient_Prescription::where([
            ['prescription_id', $request->prescription_id],
            ['ingridient_id', $request->ingredient_id]
        ])
            ->update([
                'count' => $request->count_data
            ]);
    }
}
