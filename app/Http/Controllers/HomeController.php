<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parse;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

    public function parse()
    {
        $parse = new Parse();
        $result = $parse->curl("https://telemart.ua/");
        preg_match_all('/<div class=\"b-head">(.*)<\/div>/iU', $result['content'],$categories);
        preg_match_all('/<li><a href=\"(.*)">(.*)<\/a><\/li>/iU', $result['content'],$subcategories);
        $result = $parse->curl((stristr(substr($subcategories[0][0],13),'"',true)));
        preg_match_all('/<a href=\"https:\/\/telemart.ua\/products\/(.*)\/" title=\"(.*)">(.*)<\/a>/iU', $result['content'],$products);
        return view('parse', [
            'categories' => $categories,
            'subcategories' => $subcategories,
            'products' => $products
        ]);
    }
}