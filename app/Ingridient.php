<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingridient extends Model
{
    public function prescriptions()
    {
        return $this->belongsToMany('App\Prescription', 'ingridient_prescriptions')
            -> withPivot ( 'dimension_id', 'count' )
            -> withTimestamps () ;
    }
}
