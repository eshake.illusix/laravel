<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    public function ingredients()
    {
        return $this->belongsToMany('App\Ingridient', 'ingridient_prescriptions')
            -> withPivot ( 'dimension_id', 'count' )
            -> withTimestamps ();
    }

    public function dimensions()
    {
        return $this->belongsToMany('App\Dimension', 'ingridient_prescriptions');
    }

    public function ingridient_prescriptions()
    {
        return $this->hasMany('App\Ingridient_Prescription');
    }
}
