<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingridient_Prescription extends Model
{
    protected $table = 'ingridient_prescriptions';

    public function scopePrescription_Id($query, $id)
    {
        return $query->where('prescription_id', '=', $id);
    }

    public function scopeIngridient_Id($query, $id)
    {
        return $query->where('ingridient_id', '=', $id);
    }
}
