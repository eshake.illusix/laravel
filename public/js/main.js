function User(data) {

	//user data
	var _name = data.name,
			_email = data.email,
			_password = data.password,
			_index = data.index;

	//assembly our user data to an object
	function convertToObj() {
		return {
					name: _name,
					email: _email,
					password: _password,
					index: _index
		};
	}

	function serializeData() {
		var dataObj = convertToObj();
		var serializedDataObj = JSON.stringify(dataObj);

		return serializedDataObj;
	}

	this.addToLocalStorage = function() {
		localStorage.setItem(_index, serializeData());

		var currentSize = localStorage.getItem("users_number");
		localStorage.setItem("users_number", ++currentSize);
	}

	//add hide/show , delete button to user block
	function addButtons(elem) {

		//create elements
		var buttons = document.createElement("div"),
				hideButton = document.createElement("div"),
				delButton = document.createElement("div");

		buttons.setAttribute("class", "buttons");
		hideButton.setAttribute("class", "hide")
		delButton.setAttribute("class", "del")

		hideButton.innerHTML = "HIDE";
		delButton.innerHTML = "DELETE";

		buttons.appendChild(hideButton);
		buttons.appendChild(delButton);

		//set an event handler to buttons block
		buttons.addEventListener("click", function(e) {

			var user = e.target.parentNode.parentNode;
			var button = e.target;

			//Hide/show data
			if (button.classList.contains("hide")) {
				var list = user.childNodes[0].childNodes;
				for (var i = 1; i < list.length; i++)
					list[i].classList.toggle("hidden");

				switch (button.innerHTML) {
					case "HIDE":
						button.innerHTML = "SHOW";
						break;
					case "SHOW":
						button.innerHTML = "HIDE";
						break;
				}
			}

			//delete user block from list, localStorage
			//change indeces, sort them
			if (button.classList.contains("del")) {

				//index of the deleted user
				var removeUserIndex = +user.dataset.index;

				//get the current number of items
				var currentSize = localStorage.getItem("users_number");

				//set new indeces in localStorage
				for (var i = removeUserIndex + 1; i < currentSize; i++){
					var tempObj = localStorage.getItem(i);
					localStorage.setItem(i-1, tempObj);
				}

				//set new indeces on the page
				var users = document.querySelectorAll(".user");
				for (var i = removeUserIndex + 1; i < users.length; i++)
					users[i].dataset.index = i - 1;

				//remove elem from page and localStorage
				user.parentNode.removeChild(user);
				localStorage.removeItem(currentSize - 1);

				//set new size
				localStorage.setItem("users_number", --currentSize);
			}
		})

		elem.appendChild(buttons)
	//	hideButton.click();
	}

	//create DOM element , wrap user data , div > ul > li
	this.addToList = function(listUrl) {

		var elem = document.createElement("div");
		var	elemUl = document.createElement("ul");

		elem.setAttribute("class","user");
		elem.setAttribute("data-index", _index);

		var dataObj = convertToObj();
		for (var prop in dataObj)
		{
			if (prop == "index") continue;

			var elemLi = document.createElement("li");

			elemLi.innerHTML = prop + " — " + dataObj[prop] + "";
			elemUl.appendChild(elemLi);
		}

		elem.appendChild(elemUl);
		addButtons(elem);

		listUrl.appendChild(elem);
	}
}

//clear field on click


//function evoke the callback and change variable's state -> alert the msg




function create_select() {


}

/*var val = "Fish";
var sel = document.getElementById('sel');
document.getElementById('btn').onclick = function() {
	var opts = sel.options;
	for (var opt, j = 0; opt = opts[j]; j++)
	{ if (opt.value == val)
	{ sel.selectedIndex = j;
	break; } } }
*/




