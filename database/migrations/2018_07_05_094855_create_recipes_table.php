<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prescriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->timestamps();
        });
        Schema::create('ingridients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('dimensions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        Schema::create('ingridient_prescriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prescription_id')->unsigned()->index();
            $table->integer('ingridient_id')->unsigned()->nullable();;
            $table->integer('dimension_id')->unsigned();
            $table->integer('count');
            $table->foreign('prescription_id')->references('id')->on('prescriptions')->onDelete('cascade');
            $table->foreign('ingridient_id')->references('id')->on('ingridients')->onDelete('set null');
            $table->foreign('dimension_id')->references('id')->on('dimensions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingridient_prescriptions');
        Schema::dropIfExists('prescriptions');
        Schema::dropIfExists('ingridients');
        Schema::dropIfExists('dimension');
    }
}
